# GitLabCiCd

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## CI/CD

- dependencies
- testing
- build
- deploy

### 1

```js
// karma.config.js
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},
```

### 2

```json
// npm script
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```

### 3

```bash
$ git checkout -b <new branch> origin/<new branch>
```

### 4

Syntax yml:
image: <docker_image>
<name_job>:
stage: <name_stage>
script: - <command>

```yml
# .gitlab-ci.yml
image: node:14.15-stretch
stages:
  - install
  - test
  - build
  - deploy
```

### 5

```yml
install:
  stage: install
  script:
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/
```

### 6

Statements : 100% ( 6/6 )
Branches : 100% ( 0/0 )
Functions : 100% ( 0/0 )
Lines : 100% ( 5/5 )

```yml
test:
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script: #install chrome
    - apt-get update
    - wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    - apt install -y ./google-chrome*.deb;
    - export CHROME_BIN=/usr/bin/google-chrome
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%'
```

### 7

```json
// nom script
"build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/GitLabCiCd/",
```

### 8

```yml
build:
  stage: build
  variables:
    BUILD_CONFIG: "production"
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master
```

### 9

```yml
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/GitLabCiCd/* ./public/
    - cp public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master
```

tmux
